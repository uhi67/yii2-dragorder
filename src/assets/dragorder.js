/**
 * See documentation at DragOrder.php
 */
$(function () {
	console.log('dragorder.js');

	$('.drag-target').on('dragover', function(event) {
		//console.log('dragover');
		if(event.preventDefault) event.preventDefault();
		event.originalEvent.dataTransfer.dropEffect = 'move';
		let target = this;
		let target_name = $(target).data('drag-name');
		let target_id = $(target).data('id');
		let target_ord = $(target).data('ord');

		let $context = $(this).closest(target_name ? 'div.dragorder-context[data-name="'+target_name+'"]' : 'div.dragorder-context');
		let origin = $context.data('drag-origin');
		if(!origin) {
			console.log('no origin ('+target_name+')');
			return;
		}
		let dir = $context.hasClass('drag-horizontal') ? 'horizontal' : 'vertical';
		let origin_id = $(origin).data("id");
		let origin_ord = $(origin).data("ord");
		let origin_name = $(origin).data('drag-name');
		if(origin_name!==target_name) { console.log(origin_name, target_name); return; }
		if(origin===this || $(origin).data('target_id') === target_id) { return; }

		$(origin).data('target_id', target_id);
		console.log('fixed:', origin_id, target_id, origin_ord, target_ord, origin_name, target_name);
		if(target_id && origin_id && target_id !== origin_id && target_ord < origin_ord) {
			//console.log('drag-before-'+dir);
			$(target).addClass('drag-before-'+dir);
		}
		if(target_id && origin_id && target_id !== origin_id && target_ord > origin_ord) {
			//console.log('drag-after-'+dir);
			$(target).addClass('drag-after-'+dir);
		}
		return false;
	}).on('dragenter', function() {
		this.classList.add('drag-over');
	}).on('dragleave', function() {
		let target_name = $(this).data('drag-name');
		//console.log('dragleave '+target_name);
		let $context = $(this).closest(target_name ? 'div.dragorder-context[data-name="'+target_name+'"]' : 'div.dragorder-context');
		let origin = $context.data('drag-origin');
		$(origin).data('target_id', null);

		$(this).removeClass('drag-over drag-over-vertical drag-over-horizontal');
		$(this).removeClass('drag-before drag-before-vertical drag-before-horizontal');
		$(this).removeClass('drag-after drag-after-vertical drag-after-horizontal');
	}).on('drop', function(event) {
		let target_name = $(this).data('drag-name');
		console.log('drop '+target_name);
		event.preventDefault();
		if (event.stopPropagation) {
			event.stopPropagation(); // stops the browser from redirecting.
		}
		$(this).removeClass('drag-over drag-over-vertical drag-over-horizontal');
		$(this).removeClass('drag-before drag-before-vertical drag-before-horizontal');
		$(this).removeClass('drag-after drag-after-vertical drag-after-horizontal');

		let $context = $(this).closest(target_name ? 'div.dragorder-context[data-name="'+target_name+'"]' : 'div.dragorder-context');
		let name = $context.data('name');
		let origin = $context.data('drag-origin');
		let origin_id = $(origin).data("id");
		let target_id = $(this).data('id');
		let wait = $context.data('wait');

		if(target_id && origin_id && target_id !== origin_id) {
			console.log('Dragorder is moving ', origin_id, ' to ', target_id);
			if(wait>'') {
				$context.after('<div class="wait-curtain" style="pointer-events:auto"><h1>' + wait + '</h1></div>');
			}

			let data = $context.data('params');
			if(data===undefined || data===null) data={};
			data['origin'] = origin_id;
			data['target'] = target_id;
			let url = $context.data('action');
			if(url) {
				console.log('Calling',url,data);
				// Ajax load
				$context.load(url + ' #dragorder-content-' + name, data, function () {
					// complete
					//console.log('Dragorder is completed.');
				});
			}
			else {
				// Kliens oldali átrendezés
				console.log('client reorder');
				if($(origin).data('ord') < $(this).data('ord'))
					$(origin).insertAfter($(this));
				else
					$(origin).insertBefore($(this));
				// ord átszámolása
				let o=0;
				$('.drag-target', $context).each(function () {
					$(this).data('ord', o++);
					// User event on reordering items
				});
				$context.trigger('reordered');
			}
		}
		else {
			console.log('Not moving ', origin_id, ' to ', target_id);
		}
	}).on('dragend', function() {
		// this = the dragged object
		this.style.opacity = '1';
		console.log('dragend', this);
		$(this).parents('div.dragorder-context').removeData('drag-origin'); // Removes all drag origins of all embedded contexts
	});

	$('[draggable=true]').on('dragstart', function(event) {
		let origin_name = $(this).data('drag-name');
		let $context = $(this).closest(origin_name ? 'div.dragorder-context[data-name="'+origin_name+'"]' : 'div.dragorder-context');
		if(!$context) console.log('no context ('+origin_name+')');
		$context.data('drag-origin', this);
		this.style.opacity = '0.4';
		event.originalEvent.dataTransfer.effectAllowed = 'move';
		event.originalEvent.dataTransfer.setData("data-id", $(event.target).data('id'));
		event.originalEvent.dataTransfer.setData("data-ord", $(event.target).data('ord'));
		event.originalEvent.dataTransfer.setData("data-drag-name", $(event.target).data('drag-name'));
	})

});
