<?php

namespace uhi67\dragorder;

use yii\web\AssetBundle;

class DragOrderAsset extends AssetBundle {
	public $sourcePath = __DIR__ . '/assets';
	public $css = [
		'dragorder.less',
	];
	public $js = [
		'dragorder.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
	];
}
