<?php
/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2019. 03. 07.
 * Time: 20:14
 */

namespace uhi67\dragorder;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * # Class DragOrder
 *
 * Helper widget to change order of object with drag-and-drop operations
 *
 * This widget prepares an ajax call based on user operation, to perform the actual reordering.
 *
 * ## Usage
 *
 * 1. Enclose ordered between `DragOrder::begin()` and `DragOrder::end()` tags.
 * 2. Define url of ordering controller action in `action` parameter.
 * 		The following parameters will be posted to the action:
 *		- origin -- id of moved object defined in data-id of draggable object
 *		- target -- id of target object defined in data-id of draggable object
 * 		The called ajax action must support new content for the enclosing dragorder envelope.
 * 3. Define other custom parameters in `params` attribute.
 * 4. Supply `draggable`, `data-id` and `data-ord` attributes on draggable objects, move on item to change cursor.
 * 5. Supply `drag-target` class on targets (usually the same objects as draggables)
 * 6. Define decoration classes in your css:
 *		- drag-over will be any drop target the object moved over
 *		- drag-before will be the drop target _before_ the moved object
 *		- drag-after will be the drop target _after_ the moved object
 *
 * Use Dragorder::move() in controller action to move the item in the array of the items.
 *
 * DragOrder options
 *
 * - action -- url of reorder action (post action)
 * 		Posted parameters:
 * 		- origin -- id of moved object (from data-id)
 * 		- target -- id of target object (from data-id)
 * 		- user-defined parameters
 *      If action is undefined, client-side reodreding will be performed. In this case, after reordering,
 *          on all drag-target element a "reordered" event will be triggered.
 * - params -- custom static parameters of action posted with the ajax call (optional)
 * - wait -- 'Please wait' message during reordering action. Default is simple grey curtain. Set to false or 0 to disable
 * - direction -- dragging direction (only for decoration classes): horizontal or vertical. Default is auto
 * - name -- Unique name for drag context, also generates a named anchor before ordered list to return to after reorder action
 *
 * @package app\widgets
 * @author Uherkovich Peter <uherkovich.peter@gmail.com>
 */
class DragOrder extends Widget {
	/** @var string $action -- action of drop operation */
	public $action;
	/** @var array $params -- user parameters of action */
	public $params;
	/** @var string $wait -- 'Please wait' message during reordering action. Default is simple grey curtain */
	public $wait = ' ';
	/** @var string $direction -- dragging direction (only for decoration classes): horizontal or vertical */
	public $direction = 'auto';
	/** @var string $name -- unique nameid of the widget instance (and anchor to navigate back to after reordering action) */
	public $name = 'dragorder-1';

	public function init() {
		DragOrderAsset::register( $this->getView() );
		parent::init();
		ob_start();
	}

	/**
	 * {@inheritdoc}
	 */
	public function run() {
		$result = ob_get_clean();

		$params['_csrf'] = Yii::$app->request->csrfToken;

		if($this->name) $result = Html::a('', null, ['name'=>$this->name, 'class'=>'dragorder-anchor']) . $result;
		return Html::tag('div', Html::tag('div', $result, ['id'=>'dragorder-content-'.$this->name]), [
			'class' => 'dragorder-context' . (($this->direction !== 'auto') ? ' drag-'.$this->direction : ''),
			'id' => 'dragorder-'.$this->name,
			'data-name' => $this->name,
			'data-params' => json_encode($this->params),
			'data-direction' => $this->direction,
			'data-action' => $this->action,
			'data-wait' => $this->wait,
		]);
	}

	/**
	 * Reorders set and moves `origin` item to target's position within the array
	 * Regenerates array keys.
	 *
	 * Options:
	 *  - string 'ordAttribute' -- attribute name for ordering objects (default is 'ord')
	 *  - string 'idAttribute' -- attribute name for identifying objects (default is 'id')
	 *  - array 'preOrder' -- attribute names for preordering (default is [$ordAttribute])
	 *  - callable 'orderFunc' -- function($a, $b) for preorder elements, default orders by $preOrder
	 *
	 * @param array $set -- array of orderable objects (must have $ordAttribute and $idAttribute properties)
	 * @param int $origin_id -- id of moved object
	 * @param int $target_id -- id of object at target position
	 * @param array $options
	 */
	public static function move(&$set, $origin_id, $target_id, $options=[]) {
		$ordAttribute = ArrayHelper::getValue($options, 'ordAttribute', 'ord');
		$idAttribute = ArrayHelper::getValue($options, 'idAttribute', 'id');
		$preOrder = ArrayHelper::getValue($options, 'preOrder', [$ordAttribute]);
		$orderFunc = ArrayHelper::getValue($options, 'orderFunc', function($a, $b) use($preOrder) {
			foreach ($preOrder as $ordAttribute) {
				if($a->$ordAttribute < $b->$ordAttribute) return -1;
				if($a->$ordAttribute > $b->$ordAttribute) return 1;
			}
			return 0;
		});

		usort($set, $orderFunc);
		$i = 1;
		foreach($set as $object) {
			$object->$ordAttribute = $i++;
		}

		$origin = static::array_find($set, function($a) use($idAttribute, $origin_id) { return $a->$idAttribute == $origin_id; });
		$target = static::array_find($set, function($a) use($idAttribute, $target_id) { return $a->$idAttribute == $target_id; });

		if($origin->$ordAttribute > $target->$ordAttribute) {
			// Moving backward
			$target_ord = $target->$ordAttribute;
			$middleObjects = [$target];
			foreach ($set as $o) {
				if($o->$ordAttribute > $target->$ordAttribute && $o->$ordAttribute < $origin->$ordAttribute) $middleObjects[]=$o;
			}
			foreach ($middleObjects as $o) { // Azért kell külön ciklus, mert pont az ord változik, ami válogatás alapja
				$o->$ordAttribute = $o->$ordAttribute + 1;
			}
			$origin->$ordAttribute = $target_ord;
		}
		elseif($origin->$ordAttribute < $target->$ordAttribute) {
			// Moving forward
			$middleObjects = [$target];
			foreach ($set as $o) {
				if($o->$ordAttribute > $origin->$ordAttribute && $o->$ordAttribute < $target->$ordAttribute) $middleObjects[]=$o;
			}
			foreach ($middleObjects as $o) { // Azért kell külön ciklus, mert pont az ord változik, ami válogatás alapja
				$o->$ordAttribute = $o->$ordAttribute - 1;
			}
			$origin->$ordAttribute = $target->$ordAttribute + 1;
		}
		// A végén újrarendezzük, hogy a tömbben is sorban legyenek.
		usort($set, $orderFunc);
	}

	/**
	 * Returns first element which satisfies user function.
	 * Returns null if item is not found.
	 *
	 * @param array $array
	 * @param callable $test -- function($item, $index)
	 * @return mixed|null
	 */
	public static function array_find($array, $test) {
		foreach ($array as $i=>$item) {
			if(call_user_func($test, $item, $i)) return $item;
		}
		return null;
	}

}
