DragOrder
=========

A widget for Yii2 applications for drag-and-drop reorder objects.
v1.2.2 -- Released 2021-01-06

(For v1.1.x versions see master-1.1 branch)

Prerequisites
-------------

    yii2 >= 2.0.13
    php >= 5.6

Installation
------------

The preferred way to install this extension is through composer:

    composer require uhi67/yii2-dragorder

To install development version, add to composer.json:

    "repositories": [
        ...
        {
            "type": "vcs",
            "url": "git@bitbucket.org:uhi67/yii2-dragorder.git"
        }
    ],
    "require": {
        ...   
        "uhi67/yii2-dragorder" : "dev-master"
    }

or clone from bitbucket

    git clone git@bitbucket.org:uhi67/yii2-dragorder.git

Usage
-----

### Usage in the view

1. Enclose ordered items between `DragOrder::begin()` and `DragOrder::end()` tags.
2. Define url of ordering controller action in `action` parameter.
	The following parameters will be posted to the action:
	- origin -- id of moved object defined in data-id of draggable object
	- target -- id of target object defined in data-id of draggable object
3. Define other custom parameters in `params` attribute.
4. Supply `draggable`, `data-id` and `data-ord` attributes on draggable objects
5. Supply `drag-target` class on targets (usually the same objects as draggables)
6. Customize decoration classes in your css:
	- drag-over will be any drop target the object moved over
	- drag-before will be the drop target _before_ the moved object
	- drag-after will be the drop target _after_ the moved object
	Default css indicates insertion point with thick border

### Widget attributes

- action -- url of reorder action (ajax get/post action)
    Posted parameters:
    * origin -- id of moved object (from data-id)
    * target -- id of target object (from data-id)
    * user-defined parameters
    
    If action is undefined, client-side reodreding will be performed. In this case, after reordering,
       a "reordered" event will be triggered on all drag-target element.
       
- params -- custom static parameters of action (posts with ajax action)
- wait -- 'Please wait' message during reordering action. Default is simple grey curtain. Set to false to disable
- direction -- dragging direction (only for decoration classes): horizontal or vertical. Default is auto
- name -- Unique name for drag context, also generates a named anchor before ordered list to return to after reorder action

#### Overlapping multiple drag contexts

It is possible to place multiple ordered sets overlapping each other.
The ordered sets are separate 'drag contexts' named unique by 'name' property.

### Example

#### In the wiew

	<?php DragOrder::begin([
		'name' => $dragName = 'term-list',
		'params' => ['proj'=>$model->id, 'subaction'=>'term-order'],
		'action' => Url::to(['term/order']),
		'wait' => 'Kérem, várjon',
		'direction' => 'vertical', // direction of moving of elements
	]); ?>
	<?= /** @noinspection PhpUnhandledExceptionInspection */
	GridView::widget([
		'dataProvider' => new ArrayDataProvider([
			'modelClass' => Term::class,
			'allModels' => ArrayHelper::map($model->allTerms, 'id'),
			'pagination' => false,
		]),
		'rowOptions' => function($model, $dragName) {
			return [
				'draggable' => 'true',
				'class' => 'drag-target',
				'data-id' => $model->id,
				'data-ord' => $model->num,
				'data-drag-name' => $dragName
			];
		},
		'columns' => [
			[
				'attribute' => 'num',
				'label' => Yii::t('hun', '#'),
				'options'=>['style'=>'width:45px;'],
				'contentOptions' => ['class'=>'move-vertical'],
			],
			...
		],
		]),
	]) ?>
	<?php DragOrder::end(); ?>

#### In the controller

In this example, the ordered objects (here: 'terms') holds their order in the `ord` field.

	public function actionUpdate($id) {
        $model = $this->findModel($id);
	    // Load terms
	    if($termData = $request->post('Term')) {
		    $terms = array_map(function($td) {
                return new Term(array_merge($td, [
			        'parent_id' => $model->id
				    'ord' => $index,
			    ]));
			}, $termData);
	    }
	    else {
		    $terms = $model->terms;
	    }
	    ...
	    // Do not update active records if subaction exists
	    ...
	    if($subaction=='term-order') { 
            $origin_id = Yii::$app->request->post('origin');
            $target_id = Yii::$app->request->post('target');
		    DragOrder::move($terms, $origin_id, $target_id, ['preOrder'=>['ord', 'id']]);
        }
        return $this->render('update', [
            'model' => $model,
            'terms' => $terms,
        ]);
	}

### css classes

The bold ones are you define manually, others are internals.

class name             | usage                                 
-----------------------|----------------------------------------------------------------------------------
drag-context           | wrapper div receives it automatically
drag-vertical          | generated from 'direction' property 
drag-horizontal        | -"-
drag-anchor            | generated by dragorder::begin
**drag-target**        | target lines or columns, usually "draggable"
**move**               | changes cursor to arrows automatically (necessary if no \<label>)
**move-vertical**      | changes cursor to arrows to vertical (necessary if multiple drags are embedded)
**move-horizontal**    | changes cursor to arrows to horizontal (necessary if multiple drags are embedded)
drag-over              | transient class: not used
drag-before            | transient class: displays border on target while dragging
drag-before-vertical   | transient class: displays border on target while dragging
drag-before-horizontal | transient class: displays border on target while dragging
drag-after             | -"-   
drag-after-vertical    | -"-   
drag-after-horizontal  | -"-   
wait-curtain           | displayed during ajax transaction to mask the UI


## Changes

### v1.2.2 -- Released 2021-01-06

- client side ordering, triggering user event
- empty params bugfix

### v1.2.1

- css cursor in embedded drag contexts
- checking name

### v1.2

- Removing forms
- Using ajax replace only

### v1.1.1 (master-1.1 branch is maintained separately from here)

- cursor directions
- Bugfixes

### v1.1

- Compatibility with existing form
- Multiple ordered contexts within single form
- Common array-reorder method 

### v1.0

- first release
